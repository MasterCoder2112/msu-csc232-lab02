/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Descrambler.h
 * @author Jim Daehn <jrd2112@missouristate.edu>
 * @brief  Descrambler specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef LAB02_DESCRAMBLER_H
#define LAB02_DESCRAMBLER_H

#include <cstdlib>
#include <string>

#include "Decryption.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
    /**
     * @brief   A concrete realization of the Decryption interface that employs
     *          a simple decryption scheme backed by a `std::string`.
     * @extends csc232::Decryption
     */
    class Descrambler : public Decryption {
    public:
        /**
         * Parameterized constructor that provides this Descrambler with an
         * array of character data contained in the cells defined by
         * [beginIndex, endIndex).
         *
         * @pre   beginIndex is greater than or equal to 0 and endIndex is less
         *        than or equal to MAX_SIZE.
         * @param a an initial character array stored by this Scrambler
         * @param beginIndex the index of the given array where the data begins
         * @param endIndex the index of the given array where the data ends;
         *        this is the next "free" index after the data
         * @post  The data specified by the bounds of the given array are
         *        stored in a character array. If the bounds exceed the limits
         *        of [0, MAXSIZE], they are defaulted to 0 and MAXSIZE
         *        respectively.
         */
        Descrambler(const char a[], index beginIndex, index endIndex);

        /**
         * Get the number of characters actually stored by this Descrambler.
         *
         * @return The number of characters actually stored by this Descrambler
         *         is returned, namely the difference between `end` and
         *         `begin` indexes.
         * @post   The state of this Descrambler remains unchanged after
         *         invoking this operation.
         */
        virtual size_t length() const override;

        /**
         * Return an "unscrambled" form of the characters given to this
         * Descrambler.
         *
         * @return A string form of the scrambled character data given to this
         *         Descrambler, unscrambled, is returned.
         * @post   The state of this Descrambler remains unchanged after
         *         invoking this operation.
         */
        virtual std::string unscramble() const override;

        /**
         * Return a string representation of the character data stored by this
         * Descrambler.
         *
         * @return A string representation of the character data stored by this
         *         Descrambler is returned.
         * @post   The state of this Descrambler remains unchanged after
         *         invoking this operation.
         */
        virtual std::string toString() const override;

        /**
         * @brief Descrambler destructor.
         */
        virtual ~Descrambler();

    private:
        /** The character data stored by this Descrambler. */
        std::string data;
    };
}


#endif //LAB02_DESCRAMBLER_H
