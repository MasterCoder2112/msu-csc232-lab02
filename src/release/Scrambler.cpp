/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Scrambler.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Drew Waszak <adw423@live.missouristate.edu>
 *			Alexander Byrd <zz2112@live.missouristate.edu>
 *			Moises Suazo <Moises123@live.missouristate.edu>
 * @brief   Scrambler implementation. Scrambles codes by reversing them.
 *
 * @copyright Jim Daehn, 2017. All rights reserved
 */


#include <sstream>
#include "Scrambler.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

   /**
	* @title Scrambler
	* @pre An array, a beginning index, and an index are sent into the constructor
	* @post The private char array data has the chars in a added to it from the
	* beginning index to the end index
	* @brief This constructs the data array by adding each of the indexes from
	* beginIndex to endIndex in the array "a" to data.
	* @parameters const char a[], index beginIndex, index endIndex
	*/
    Scrambler::Scrambler(const char a[], index beginIndex, index endIndex)
            : begin(beginIndex), end(endIndex) {

		for (index i = beginIndex; i <= endIndex; ++i){
			data[i] = a[i];
		}
		
    }

   /**
	* @title length
	* @pre NA
	* @post The length of the array (that's not NULL) is returned
	* @brief This finds how many non-NULL chars are present in data, and
	* returns that length.
	* @return lengthCounter (size_t is pretty much like an int so it works,
	* but it's actually the size of the object in bytes)
	*/
    size_t Scrambler::length() const {

		int lengthCounter = 0;
        
		//For all char indexes in data
		for (char temp : data) {

			//If temp is NULL then break from loop
			if (!temp) {

				break;
			}

			lengthCounter++;
		}

        return lengthCounter;
    }

   /**
	* @title scramble
	* @pre NA
	* @post The scrambled (reversed) version of the char array is returned
	* in the form of a string.
	* @brief Creates a stringstream, and the last index (that doesn't hold a NULL)
	* of the array to send along with the data array and the beginning index 0 of
	* the array to reverse to reverse chars in data using the stringstream, and then
	* casts that stringstream into a string to be returned
	* @return temp (The reversed string of chars from data)
	*/
    std::string Scrambler::scramble() const {

		std::stringstream ss;

		int endOfArray = length() - 1;

		reverse(data, ss, 0, endOfArray);

		std::string temp = ss.str();

        return temp;
    }

   /**
	* @title toString
	* @pre NA
	* @post The chars in data (that aren't NULL) are returned in order
	* @brief This takes all the chars that aren't NULL in data, and adds
	* them to a string in order to be returned, representing the data 
	* in the data array.
	* @return dataString (string of chars in data)
	*/
    std::string Scrambler::toString() const {
        
		std::string dataString = "";

		for (char temp : data) {

			//If temp is not NULL
			if (temp) {

				dataString += temp;
			}
			else {
				break;
			}
		}

        return dataString;
    }

	//Default constructor. No purpose at this time
    Scrambler::~Scrambler() {
        // No op... leave me alone.
    }

    // Private data member implementations

   /**
	* @title reverse
	* @pre An char array of some length, a stringstream, a first, and a last
	* index.
	* @post The array sent in is not changed, but the representation of it
	* using the stringstream is reversed.
	* @brief Takes the array sent in, and uses recursion to continue calling
	* the function until the integer (last) representing the last index equals
	* the first index, meaning the array has been iterated through from last to
	* first index, effectively reversing the array. Each of the indexes is just
	* input into the stringstream though, no having any effect on the array itself
	* but just how it is represented in the stringstream. The stringstream is a 
	* reference of the stringstream actually sent by another method.
	* @parameters const char anArray[], std::stringstream& ss, const int first,
	* const int last
	*/
    void Scrambler::reverse(const char anArray[], std::stringstream& ss,
                            const int first, const int last) const {

		if (first <= last) {
			ss << anArray[last];
			reverse(anArray, ss, first, last - 1);
		}
    }
}
