/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Scrambler.h
 * @authors Jim Daehn <jdaehn@missouristateedu>
 *          <FILL ME IN ACCORDINGLY>
 * @brief   Scrambler specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef SCRAMBLER_H
#define SCRAMBLER_H

#include <iostream>
#include <string>

#include "Encryption.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
    /**
     * @brief   A concrete realization of the Encryption interface that employs
     *          a simple decryption scheme backed by a raw array.
     * @extends csc232::Encryption
     */
    class Scrambler : public Encryption {
    public:
        /**
         * Parameterized constructor that provides this Scrambler with an array
         * of character data contained in the cells defined by [beginIndex,
         * endIndex).
         *
         * @param a an initial character array stored by this Scrambler
         * @param beginIndex the index of the given array where the data begins
         * @param endIndex the index of the given array where the data ends;
         *        this is the next "free" index after the data
         * @pre   beginIndex is greater than or equal to 0 and endIndex is less
         *        than or equal to MAX_SIZE.
         * @post  This Scrambler stores the subset of the given array defined
         *        by the given boundary values. If the boundary values do not
         *        meet the preconditions, they are defaulted to 0 and
         *        MAX_SIZE respectively.
         */
        Scrambler(const char a[], index beginIndex, index endIndex);

        /**
         * Get the number of characters actually stored by this Scrambler.
         *
         * @return The number of characters actually stored by this Scramber is
         *         returned, namely the difference between `end` and `begin`.
         */
        virtual size_t length() const override;

        /**
         * Return a "scrambled" form of the characters stored by this Scrambler.
         * For this lab, "scrambled" is nothing more than reversed.
         *
         * @return A string form of the character data stored by this Scrambler,
         *         in reverse order, is returned.
         * @post   The stored character data remains unchanged, i.e., a call to
         *         toString() after a call to this method would return the
         *         original character data in its original order.
         */
        virtual std::string scramble() const override;

        /**
         * Return a string representation of the character data stored by this
         * Scrambler.
         *
         * @return A string representation of the character data stored by this
         *         Scrambler is returned.
         */
        virtual std::string toString() const override;

        /**
         * Scrambler destructor.
         */
        virtual ~Scrambler();

    private:
        /**
         * A private "helper" method utilized by the scramble() operation. This
         * helper method is to be written as a recursive method. See page 67
         * for a close facsimile to this method. The only difference is that the
         * method in the book prints directly to the standard output stream;
         * this method doesn't do that. Rather, it inserts data into the given
         * `std::stringstream`. Since the `std::stringstream` is passed by
         * reference, changes that occur to it during the execution of this
         * method persist from invocation to invocation.
         *
         * @param anArray the array to reverse
         * @param ss a `std::stringstream` used as an inout parameter that
         *        will hold the scrambled data on the way out
         * @param first the index of the first character in the array
         * @param last the index that is 1 greater than the last character in the
         *        the array
         * @pre   `anArray` contains size characters, where size >= 0.
         * @post  `ss` contains the values of anArray in reverse
         */
        void reverse(const char anArray[], std::stringstream &ss,
                     const int first, const int last) const;

        /** The character data stored by this Scrambler. */
        char data[MAX_SIZE];
        /** The index of the first character in the array. */
        index begin;
        /** The index that is 1 greater than the last character in the array. */
        index end;
    };

}

#endif /* SCRAMBLER_H */
