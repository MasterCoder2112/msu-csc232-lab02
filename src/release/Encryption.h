/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Encryption.h
 * @author Jim Daehn <jrd2112@missouristate.edu>
 * @brief  Encryption specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef LAB02_ENCRYPTION_H
#define LAB02_ENCRYPTION_H

#include <string>
#include "Crypto.h"

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {
    /**
     * @brief   Abstract base class for encryption classes of the Crypto class
     *          hierarchy.
     * @extends csc232::Crypto
     */
    class Encryption : public Crypto {
    public:
        /**
         * Return a "scrambled" form of the characters stored by this
         * Encryption.
         *
         * @return A string form of the character data stored by this
         *         Encryption, in reverse order, is returned.
         * @post   The state of this Encryption instance remains unchanged after
         *         executing this operation.
         */
        virtual std::string scramble() const = 0;

        /**
         * @brief  An accessor method for the length of data stored in this
         *         Encryption instance.
         * @return The length of the data stored by this Encryption instance is
         *         returned.
         * @post   The state of this Encryption instance remains unchanged after
         *         executing this operation.
         */
        virtual size_t length() const override = 0;

        /**
         * Return a string representation of the character data stored by this
         * Encryption intance.
         *
         * @return A string representation of the character data stored by this
         *         Encryption is returned.
         * @post   The state of this Encryption instance remains unchanged after
         *         executing this operation.
         */
        virtual std::string toString() const = 0;
    };
}
#endif //LAB02_ENCRYPTION_H
